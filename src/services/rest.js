import Axios from 'axios'

import store from '../store'

import { PRELOADER_SHOW, PRELOADER_HIDE } from '../data/actions-types'

export default {
  context: null,
  configure (context) {
    this.context = context
  },
  onError (error) {
    console.error(error)
  },
  async sendRequest (method = 'get', uri = '/', payload = {}, silent = false) {
    if (!silent) {
      store.dispatch(PRELOADER_SHOW)
    }
    return Axios({
      method,
      url: this.context + uri,
      data: payload,
      headers: {
        'Access-Control-Allow-Origin': '*'
      }
    }).then(({ data, status }) => {
      if (!silent) {
        store.dispatch(PRELOADER_HIDE)
      }
      return { data, status }
    }).catch((error) => {
      if (!silent) {
        store.dispatch(PRELOADER_HIDE)
      }
      this.onError(error)
      return { data: {}, status: error.response.status }
    })
  },
  async get (uri, payload) {
    return this.sendRequest('get', uri, payload)
  },
  async post (uri, payload) {
    return this.sendRequest('post', uri, payload)
  },
  async put (uri, payload) {
    return this.sendRequest('put', uri, payload)
  },
  async delete (uri) {
    return this.sendRequest('delete', uri)
  }
}
