import Vue from 'vue'
import Vuex from 'vuex'

import auth from './modules/auth'
import config from './modules/config'
import tasks from './modules/tasks'
import preloader from './modules/preloader'
import notifications from './modules/notifications'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    auth,
    config,
    tasks,
    preloader,
    notifications
  }
})

window.store = store

export default store
