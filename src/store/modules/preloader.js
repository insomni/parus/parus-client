import { PRELOADER_SHOW, PRELOADER_HIDE } from '../../data/actions-types'

const TIMEOUT_HIDE = 500

export default {
  state: {
    visible: false,
    timeoutId: null
  },
  getters: {
    preloader: (state) => {
      return state.visible
    }
  },
  mutations: {
    [PRELOADER_SHOW]: (state) => {
      clearTimeout(state.timeoutId)
      state.timeoutId = null
      state.visible = true
    },
    [PRELOADER_HIDE]: (state) => {
      clearTimeout(state.timeoutId)
      state.timeoutId = setTimeout(() => {
        state.visible = false
      }, TIMEOUT_HIDE)
    }
  },
  actions: {
    [PRELOADER_SHOW]: ({ commit }) => {
      commit(PRELOADER_SHOW)
    },
    [PRELOADER_HIDE]: ({ commit }) => {
      commit(PRELOADER_HIDE)
    }
  }
}
