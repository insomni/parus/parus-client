import { CONFIG_SET_ACTIVE_LINK } from '../../data/actions-types'

export default {
  state: {
    activeLink: null
  },
  getters: {
    activeLink: (state) => {
      return state.activeLink
    }
  },
  mutations: {
    [CONFIG_SET_ACTIVE_LINK]: (state, payload) => {
      state.activeLink = payload
    }
  },
  actions: {
    [CONFIG_SET_ACTIVE_LINK]: ({ commit }, payload) => {
      commit(CONFIG_SET_ACTIVE_LINK, payload)
    }
  }
}
