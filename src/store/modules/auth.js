import Cookies from 'js-cookie'

import rest from '../../services/rest'

import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_GUEST, AUTH_SIGN } from '../../data/actions-types'
import { COOKIE_USER, COOKIE_GUEST } from '../../data/cookie-names'

import uuidv4 from 'uuid/v4'

const COOKIE_EXPIRES = 30 // days

export default {
  state: {
    user: Cookies.getJSON(COOKIE_USER) || null,
    guestCredentials: Cookies.getJSON(COOKIE_GUEST) || null
  },
  getters: {
    isLoggedIn: (state) => {
      return !!state.user
    },
    username: (state) => {
      return state.user && state.user.username
    },
    userId: (state) => {
      return state.user && state.user.uid
    },
    isManager: (state) => {
      return state.user && state.user.user_group > 50
    },
    isGuest: (state) => {
      return state.user && state.user.is_guest === true
    }
  },
  mutations: {
    [AUTH_LOGIN]: (state, payload) => {
      state.user = payload
    },
    [AUTH_LOGOUT]: (state) => {
      state.user = null
    },
    [AUTH_GUEST]: (state, payload) => {
      state.guestCredentials = payload
    }
  },
  actions: {
    [AUTH_LOGIN]: async ({ commit }, payload) => {
      return rest.post('/login', {
        'username': payload.username,
        'password': payload.password
      }).then(({ data, status }) => {
        if (status === 200) {
          commit(AUTH_LOGIN, data)
          Cookies.set(COOKIE_USER, data, { expires: COOKIE_EXPIRES })
          return true
        }
        return false
      })
    },
    [AUTH_LOGOUT]: ({ commit }) => {
      commit(AUTH_LOGOUT)
      Cookies.remove(COOKIE_USER)
    },
    [AUTH_SIGN]: async (context, payload) => {
      return rest.post('/users/', {
        username: payload.username,
        password: payload.password,
        is_guest: true
      }).then(({ status }) => {
        if (status === 201) {
          return true
        }
        return false
      })
    },
    [AUTH_GUEST]: async ({ commit, state, dispatch }) => {
      let guestCredentials = {}
      if (state.guestCredentials) {
        return dispatch(AUTH_LOGIN, state.guestCredentials)
      } else {
        guestCredentials.username = uuidv4()
        guestCredentials.password = uuidv4()
        return dispatch(AUTH_SIGN, guestCredentials).then((res) => {
          if (res) {
            Cookies.set(COOKIE_GUEST, guestCredentials, { expires: COOKIE_EXPIRES })
            commit(AUTH_GUEST, guestCredentials)
            return dispatch(AUTH_LOGIN, guestCredentials)
          } else {
            return false
          }
        })
      }
    }
  }
}
