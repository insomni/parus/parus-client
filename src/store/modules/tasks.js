import moment from 'moment'

import rest from '../../services/rest'

import { COMMENTS_DELETE, COMMENTS_LOAD, TASKS_LOAD_ALL,
  TASKS_ADD, TASKS_SELECT, COMMENTS_ADD, TASKS_UPDATE,
  TASKS_SELECT_BY_ID } from '../../data/actions-types'

export default {
  state: {
    tasks: [],
    currentTask: null,
    comments: []
  },
  getters: {
    allTasks: (state) => {
      return state.tasks
    },
    currentTask: (state) => {
      return state.currentTask
    },
    activeTasks: (state) => {
      return state.tasks.filter((task) => { return !task.dt_end })
    },
    doneTasks: (state) => {
      return state.tasks.filter((task) => { return !!task.dt_end })
    },
    selfTasks: (state, getters) => {
      return state.tasks.filter((task) => { return task.creator.uid === getters.userId })
    },
    comments: (state) => {
      return state.comments
    }
  },
  mutations: {
    [TASKS_SELECT]: (state, payload) => {
      state.currentTask = payload
    },
    [TASKS_LOAD_ALL]: (state, payload) => {
      state.tasks = payload.sort((task) => { return moment(task.dt_start).unix() })
    },
    [TASKS_UPDATE]: (state, payload) => {
      state.currentTask = payload
    },
    [COMMENTS_LOAD]: (state, payload) => {
      state.comments = payload
    }
  },
  actions: {
    [TASKS_SELECT]: ({ commit }, payload) => {
      commit(TASKS_SELECT, payload)
    },
    [TASKS_SELECT_BY_ID]: ({ commit, state }, payload) => {
      commit(TASKS_SELECT, state.tasks.find((task) => { return task.uid === payload }))
    },
    [TASKS_LOAD_ALL]: ({ commit }) => {
      return rest.get('/tasks/').then(({ data, status }) => {
        if (status === 200) {
          commit(TASKS_LOAD_ALL, data)
        }
      })
    },
    [TASKS_ADD]: async ({ getters }, payload) => {
      return rest.post('/tasks/', {
        'data': payload,
        'dt_start': moment().format(),
        'status': 0,
        'creator_id': getters.userId
      }).then(({ status }) => {
        return status === 201
      })
    },
    [TASKS_UPDATE]: async ({ commit }, payload) => {
      return rest.put('/tasks/', payload).then(({ status }) => {
        if (status === 200) {
          commit(TASKS_UPDATE, payload)
        }
      })
    },
    [COMMENTS_LOAD]: async ({ state, commit }) => {
      if (state.currentTask) {
        return rest.get('/comments/task/' + state.currentTask.uid).then(({ data, status }) => {
          if (status === 200) {
            commit(COMMENTS_LOAD, data)
          }
        })
      }
    },
    [COMMENTS_ADD]: async ({ dispatch }, payload) => {
      return rest.post('/comments/', {
        text: payload.text,
        type: payload.type || 1,
        creator_id: payload.creator_id,
        task_id: payload.task_id
      }).then(({ status }) => {
        if (status === 201) {
          return dispatch(COMMENTS_LOAD)
        }
      })
    },
    [COMMENTS_DELETE]: async ({ dispatch }, payload) => {
      return rest.delete('/comments/' + payload).then(({ status }) => {
        if (status === 204) {
          return dispatch(COMMENTS_LOAD)
        }
      })
    }
  }
}
