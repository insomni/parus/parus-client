import rest from '../../services/rest'

import { NOTIFICATIONS_LOAD, NOTIFICATIONS_MARK_ALL, NOTIFICATIONS_MARK_ONE } from '../../data/actions-types'

export default {
  state: {
    notifications: []
  },
  getters: {
    notifications: (state) => {
      return state.notifications
    },
    unreadNotifications: (state) => {
      return state.notifications.filter((notification) => { return notification.is_active })
    }
  },
  mutations: {
    [NOTIFICATIONS_LOAD]: (state, payload) => {
      state.notifications = payload
    }
  },
  actions: {
    [NOTIFICATIONS_LOAD]: async ({ getters, commit }, payload) => {
      return rest.sendRequest('get', '/notifications/user/' + getters.userId, {}, !!payload).then(({ data, status }) => {
        if (status === 200) {
          commit(NOTIFICATIONS_LOAD, data)
        }
      })
    },
    [NOTIFICATIONS_MARK_ALL]: async ({ getters, dispatch }) => {
      return rest.put('/notifications/user/' + getters.userId + '/deactivate').then(({ status }) => {
        if (status === 200) {
          dispatch(NOTIFICATIONS_LOAD, false)
        }
      })
    },
    [NOTIFICATIONS_MARK_ONE]: async ({ dispatch }, payload) => {
      return rest.sendRequest('put', '/notifications/', {
        ...payload,
        user_id: payload.user.uid,
        task_id: payload.task.uid
      }, true).then(({ status }) => {
        if (status === 200) {
          dispatch(NOTIFICATIONS_LOAD, false)
        }
      })
    }
  }
}
