import Vue from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import moment from 'moment'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faPlus, faBell, faSync } from '@fortawesome/free-solid-svg-icons'
import { faBell as faBellRegular } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import './assets/fonts.scss'

// Store
import store from './store'

// Services
import l10n from './services/l10n'
import rest from './services/rest'

moment.locale('ru')

library.add(faPlus, faBell, faBellRegular, faSync)

Vue.config.productionTip = false
rest.configure('http://localhost:5000')

Vue.use(BootstrapVue)

Vue.component('fa-icon', FontAwesomeIcon)

Vue.mixin({
  created: function () {
    this._ = l10n
    this.$rest = rest
  }
})

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
