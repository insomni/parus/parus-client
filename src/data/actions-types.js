export const AUTH_LOGIN = 'p.auth.login'
export const AUTH_LOGOUT = 'p.auth.logout'
export const AUTH_GUEST = 'p.auth.guest'
export const AUTH_SIGN = 'p.auth.sign'

export const CONFIG_SET_ACTIVE_LINK = 'p.config.set_active_link'

export const PAGE_NEW_TASK = 'p.page.new_task'
export const PAGE_ACTIVE_TASK = 'p.page.active_tasks'
export const PAGE_DONE_TASK = 'p.page.done_tasks'
export const PAGE_SELF_TASK = 'p.page.self_tasks'
export const PAGE_CURRENT_TASK = 'p.page.current_task'

export const TASKS_SELECT = 'p.tasks.select'
export const TASKS_SELECT_BY_ID = 'p.tasks.select_by_id'
export const TASKS_LOAD_BY_ID = 'p.tasks.load_by_id'
export const TASKS_LOAD_ALL = 'p.tasks.load_all'
export const TASKS_ADD = 'p.tasks.add'
export const TASKS_UPDATE = 'p.tasks.update'
export const TASKS_DELETE = 'p.tasks.delete'

export const COMMENTS_LOAD = 'p.comments.load'
export const COMMENTS_ADD = 'p.comments.add'
export const COMMENTS_DELETE = 'p.comments.delete'

export const NOTIFICATIONS_LOAD = 'p.notifications.load'
export const NOTIFICATIONS_MARK_ALL = 'p.notifications.mark_all'
export const NOTIFICATIONS_MARK_ONE = 'p.notifications.mark_one'

export const PRELOADER_SHOW = 'p.preloader.show'
export const PRELOADER_HIDE = 'p.preloader.hide'
